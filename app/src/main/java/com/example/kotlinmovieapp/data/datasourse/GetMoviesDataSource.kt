package com.example.kotlinmovieapp.data.datasourse

import com.example.kotlinmovieapp.network.datamodels.MovieResponseModel
import retrofit2.Response

interface GetMoviesDataSource {
    suspend fun getMovies(): Response<List<MovieResponseModel>>
}