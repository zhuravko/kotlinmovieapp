package com.example.kotlinmovieapp.data.repository

import com.example.kotlinmovieapp.data.datasourse.GetMoviesDataSource
import com.example.kotlinmovieapp.domain.repository.GetMoviesRepository
import com.example.kotlinmovieapp.network.datamodels.MovieResponseModel
import com.example.kotlinmovieapp.network.datasource.GetMoviesDataSourceImpl
import retrofit2.Response

class GetMoviesRepositoryImpl: GetMoviesRepository {

    private val dataSource: GetMoviesDataSource = GetMoviesDataSourceImpl()

    override suspend fun getMovies(): Response<List<MovieResponseModel>> {
        return dataSource.getMovies()
    }
}