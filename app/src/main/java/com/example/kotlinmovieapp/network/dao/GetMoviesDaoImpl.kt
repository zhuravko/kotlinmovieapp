package com.example.kotlinmovieapp.network.dao

import com.example.kotlinmovieapp.network.RetrofitService
import com.example.kotlinmovieapp.network.datamodels.MovieResponseModel
import retrofit2.Response

class GetMoviesDaoImpl: GetMoviesDao {
    override suspend fun getMovies(): Response<List<MovieResponseModel>> {
        val retrofitService = RetrofitService.getInstance()
        return retrofitService.getMovies()
    }
}