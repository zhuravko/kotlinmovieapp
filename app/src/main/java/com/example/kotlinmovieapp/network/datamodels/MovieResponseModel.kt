package com.example.kotlinmovieapp.network.datamodels

data class MovieResponseModel(val name: String, val imageUrl: String, val category: String, val desc: String)