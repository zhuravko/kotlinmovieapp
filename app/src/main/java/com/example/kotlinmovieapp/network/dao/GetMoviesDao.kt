package com.example.kotlinmovieapp.network.dao

import com.example.kotlinmovieapp.network.datamodels.MovieResponseModel
import retrofit2.Response

interface GetMoviesDao {
    suspend fun getMovies(): Response<List<MovieResponseModel>>
}