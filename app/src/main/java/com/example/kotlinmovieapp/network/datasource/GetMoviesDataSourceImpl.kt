package com.example.kotlinmovieapp.network.datasource

import com.example.kotlinmovieapp.data.datasourse.GetMoviesDataSource
import com.example.kotlinmovieapp.network.dao.GetMoviesDao
import com.example.kotlinmovieapp.network.dao.GetMoviesDaoImpl
import com.example.kotlinmovieapp.network.datamodels.MovieResponseModel
import retrofit2.Response

class GetMoviesDataSourceImpl: GetMoviesDataSource {

    private val moviesDao: GetMoviesDao = GetMoviesDaoImpl()
    override suspend fun getMovies(): Response<List<MovieResponseModel>> {
        return moviesDao.getMovies()
    }
}