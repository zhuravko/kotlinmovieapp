package com.example.kotlinmovieapp.presentation.ui.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kotlinmovieapp.network.datamodels.MovieResponseModel
import com.example.kotlinmovieapp.R

class MoviesAdapter() : RecyclerView.Adapter<MoviesAdapter.MyViewHolder>() {

    private var context: Context? = null;
    private var movieResponseModels: List<MovieResponseModel> = ArrayList()

    constructor(context: Context?, movieResponseModels: List<MovieResponseModel>) : this(){
        this.context = context
        this.movieResponseModels = movieResponseModels
    }
    constructor(context: Context?) : this(){
        this.context = context
    }

    fun setMovies(list:List<MovieResponseModel>){
        movieResponseModels = list
        Log.e("DAS", list.toString())
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView? = null
        var description: TextView? = null
        var imageMovie: ImageView? = null

        init {
            title = itemView.findViewById(R.id.title)
            description = itemView.findViewById(R.id.description)
            imageMovie = itemView.findViewById(R.id.image_movie)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val movie = movieResponseModels[position]
            holder.title?.text = movie.name;
            holder.description?.text = movie.desc
            Glide
                .with(context!!)
                .load(movie.imageUrl)
                .into(holder.imageMovie!!)

    }

    override fun getItemCount(): Int {
       return movieResponseModels.size
    }
}