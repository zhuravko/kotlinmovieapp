package com.example.kotlinmovieapp.presentation.ui.activities

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlinmovieapp.databinding.ActivityMainBinding
import com.example.kotlinmovieapp.presentation.ui.adapters.MoviesAdapter
import com.example.kotlinmovieapp.presentation.viewmodels.MainViewModel

class MainActivity : AppCompatActivity() {

    private var adapter = MoviesAdapter(this)
    lateinit var viewModel: MainViewModel
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.listMovies.layoutManager = LinearLayoutManager(this)
        binding.listMovies.adapter = adapter

        viewModel = MainViewModel()
        viewModel.movieList.observe(this) {
            adapter.setMovies(it)
        }
        viewModel.errorMessage.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }
        viewModel.loading.observe(this, Observer {
            if (it) {

            } else {

            }
        })
        viewModel.getAllMovies()
    }

}