package com.example.kotlinmovieapp.domain.repository

import com.example.kotlinmovieapp.network.datamodels.MovieResponseModel
import retrofit2.Response

interface GetMoviesRepository {
    suspend fun getMovies(): Response<List<MovieResponseModel>>
}