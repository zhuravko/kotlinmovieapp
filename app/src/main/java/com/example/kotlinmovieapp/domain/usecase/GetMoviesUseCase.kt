package com.example.kotlinmovieapp.domain.usecase

import com.example.kotlinmovieapp.network.datamodels.MovieResponseModel
import retrofit2.Response

interface GetMoviesUseCase {
    suspend fun getMovies(): Response<List<MovieResponseModel>>
}