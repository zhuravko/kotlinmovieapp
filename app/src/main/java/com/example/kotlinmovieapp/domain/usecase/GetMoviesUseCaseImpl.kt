package com.example.kotlinmovieapp.domain.usecase

import com.example.kotlinmovieapp.data.repository.GetMoviesRepositoryImpl
import com.example.kotlinmovieapp.domain.repository.GetMoviesRepository
import com.example.kotlinmovieapp.network.datamodels.MovieResponseModel
import retrofit2.Response

class GetMoviesUseCaseImpl: GetMoviesUseCase {

    private val repositoryImpl: GetMoviesRepository = GetMoviesRepositoryImpl()

    override suspend fun getMovies(): Response<List<MovieResponseModel>> {
        return repositoryImpl.getMovies()
    }
}